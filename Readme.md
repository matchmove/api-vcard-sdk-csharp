﻿C# (6.0) SDK
==============

- version: 1.0
- compatible with: API version 1.3
- __Minimum__ C# version: 6.0

Required Packages:
------------------

- [Json.NET](http://www.newtonsoft.com/json)

Dependencies:
-------------
- [Json.NET](http://www.newtonsoft.com/json)


Installation:
-------------

1. Install this repository to your project directory.
    
        git clone https://bitbucket.org/matchmove/api-vcard-sdk-csharp Projects/OP/OP

2. Install the Json.NET package by downloading from their site or NuGET. 

Usage:
------



### Initializing a `Connection` object

    :::c#
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    
### Consuming public resources

    :::c#
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    
    // List all card types.
    JObject cardTypes = wallet.Consume("users/wallets/cards/types");
    
    // Register a user.
    IDictionary<String, String> userData = new Dictionary<String, String> ();
    
    userData.Add ("email", email);
    userData.Add ("password", password);
    userData.Add ("first_name", "John");
    userData.Add ("last_name", "Doe");
    userData.Add ("mobile_country_code", "65");
    userData.Add ("mobile", "98765432");
    
    JObject user = wallet.consume("users", "POST", userData);
    
### "Logging in" / User authentication

    :::c#
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    wallet.Authenticate(user, password);
    

### Consume Authenticated resources

    :::c#
    Connection wallet = new Connection(host, consumerKey, consumerSecret);
    wallet.Authenticate(user, password);
    
    // Get user details
    JObject user = wallet.Consume("users")
    
    // Update user's details
	IDictionary<String, String> userData = new Dictionary<String, String> ();
	
	userData.put ("title", "Mr");
	userData.put ("gender", "male");
	userData.put ("id_number", "s1234567890");
	userData.put ("id_type", "nric");
	userData.put ("country_of_issue", "Singapore");
			
    JObject userUpdated = wallet.consume("users", "PUT", userData);

More Examples:
--------------

- [Demo.cs](https://bitbucket.org/matchmove/api-vcard-sdk-csharp/downloads/Demo.cs)

Issues
------

[Any inquiries, issues or feedbacks?](https://bitbucket.org/matchmove/api-vcard-sdk-csharp/issues)
