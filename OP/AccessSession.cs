﻿using System;

namespace OP
{
	public class AccessSession
	{
		private string host = null;
		private string key = null;
		private string secret = null;
		private IConnectionSession session = null;

		public AccessSession (string host, string key, string secret, IConnectionSession session)
		{
			this.host = host;
			this.key = key;
			this.secret = secret;
			this.session = session;
		}

		public string Host()
		{
			return this.host;
		}

		public string Key()
		{
			return this.key;
		}

		public string Secret()
		{
			return this.secret;
		}

		public string Session()
		{
			return this.session.ToString();
		}

		public string SessionKey(string user)
		{
			byte[] data = Tools.MD5Byte (host + key + user);
			return Tools.MD5String (data);
		}

		public void Set(string user, string value)
		{
			session.Set (SessionKey (user), value);
		}

		public string Get(string user)
		{
			return session.Get (SessionKey (user));
		}

		public void Unset(string user)
		{
			session.Set (SessionKey (user), null);
		}
	}
}

