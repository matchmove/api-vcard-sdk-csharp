﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace OP
{
	class Signature {
		// These may be deprecated properties as they aren't useful for this C# version
		public static readonly string HASH = "HmacSHA1";
		public static readonly string Method = "HMAC-SHA1";

		private string signature = "";

		public Signature (string secret, string method, string host, IDictionary<string, string> payload) 
		{
			IDictionary<string, string> sortedPayload = new SortedDictionary<string, string> (payload);

			string rawSignature = Tools.Escape(method) + "&"
				+ Tools.Escape (host) + "&"
				+ Tools.Escape (Tools.DictionaryToString (sortedPayload));

			Tools.Log ("RAW SIGNATURE", rawSignature); 

			signature = Tools.Hash(rawSignature, secret);

			Tools.Log ("Encrypted SIGNATURE", signature);
		}

		public string String()
		{
			return signature;
		}
	}
}

