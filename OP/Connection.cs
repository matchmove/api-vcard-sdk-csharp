﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

namespace OP
{
	class Connection {

		private string host   = null; 
		private string key    = null;
		private string secret = null;
		private AccessToken accessToken = null;
		protected AccessSession session = null;

		public static readonly int OK = 200;

		public Connection (string host, string key, string secret, string ssl_certificate_path) 
		{
			Initialize (host, key, secret);
		}

		public Connection (string host, string key, string secret)
		{
			Initialize (host, key, secret);
		}

		public void Initialize (string host, string key, string secret) 
		{
			this.host = host;
			this.key = key;
			this.secret = secret;
			this.accessToken = new AccessToken (host, key, secret);
		}

		public void Authenticate (string user, string password)
		{
			if (this.session != null) {
				this.accessToken = new AccessToken (this.session, user);
			}

			if (this.accessToken.Response == null) {
				var requestToken = new RequestToken (this.host, this.key, this.secret, user, password);
				this.accessToken = new AccessToken (requestToken);

				if (this.session != null) {
					this.session.Set (user, this.accessToken.Response.ToString ());
				}
			}

		}

		public Boolean Authenticate (string user)
		{
			if (this.session != null) {
				this.accessToken = new AccessToken (this.session, user);
			}

			return this.accessToken.Response != null;
		}

		public static string Hash (byte[] data)
		{
			var hash = Tools.MD5Byte (Encoding.ASCII.GetString (data));

			return Encoding.ASCII.GetString (hash);
		}

		public void SetSession(IConnectionSession session)
		{
			this.session = new AccessSession (this.host, this.key, this.secret, session);
		}

		public JObject Consume (string resource) 
		{
			return this.Consume (resource, "GET", null);
		}

		public JObject Consume (string resource, string method) 
		{
			return this.Consume (resource, method, null);
		}

		public JObject Consume (string resource, string method, IDictionary<string, string> payload) 
		{
			return accessToken.Consume (resource, method, payload);
		}

		public static JObject Request(string method, string url, IDictionary<string, string> payload)
		{
			string contents = Tools.DictionaryToString (payload);
			method = method.ToUpper ();
			JObject serviceReply = new JObject ();

			if (method != "POST") {
				url = url + "?" + contents;
			}

			Tools.Log ("Request URL", url);
			Tools.Log ("Request Method", method);
			Tools.Log ("Request Payload", contents);

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.Method = method.ToUpper();
			request.ContentType = "application/x-www-form-urlencoded";

			if (method == "POST") {
				using (Stream stream = request.GetRequestStream())
				{
					byte[] content = Encoding.UTF8.GetBytes(contents);
					stream.Write(content, 0, content.Length);
				}
			}

			try
			{
				using (WebResponse response = request.GetResponse())
				{
					using (Stream data = response.GetResponseStream())
					using (var reader = new StreamReader(data))
					{
						string responseText = reader.ReadToEnd();
						Tools.Log ("Response Contents", responseText);

						try 
						{
							serviceReply = JObject.Parse(responseText);
						}
						catch (Exception jex)
						{
							Tools.Log ("Invalid JSON returned", jex.ToString());
						}

					}
				}
			}
			catch (WebException e)
			{
				using (WebResponse response = e.Response)
				{
					HttpWebResponse httpResponse = (HttpWebResponse) response;
					Tools.Log ("Error code", httpResponse.StatusCode.ToString());
					Tools.Log ("Error message", httpResponse.StatusDescription.ToString());
					using (Stream data = response.GetResponseStream())
					using (var reader = new StreamReader(data))
					{
						serviceReply.Add ("status_code", httpResponse.StatusCode.ToString());
						serviceReply.Add ("status_description", httpResponse.StatusDescription.ToString());
					}
				}
			}

			return serviceReply;
		}
	}
}