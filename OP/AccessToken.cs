﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

namespace OP
{
	class AccessToken : Token {
		private string method = "POST";
		private string api = "/oauth/access/token";

		public AccessToken(string host, string key, string secret)
		{
			this.Initialize (host, key, secret);
			this.Response = null;
		}

		public AccessToken(AccessSession session, string user)
		{
			this.Initialize (session.Host (), session.Key (), session.Secret ());

			string cache = session.Get(user);

			this.Response = cache == null ? null : new JObject (cache);
		}

		public AccessToken(RequestToken requestToken)
		{
			this.Initialize (requestToken.Host, requestToken.Key, requestToken.Secret);

			string url = this.Host + this.api;

			JObject oauthToken = requestToken.Response;

			IDictionary<string, string> payload = new Dictionary<string, string>();
			payload.Add("oauth_consumer_key", this.Key);
			payload.Add("oauth_nonce", OAuth.Nonce());
			payload.Add("oauth_signature_method", Signature.Method);
			payload.Add("oauth_timestamp", OAuth.Timestamp().ToString());
			payload.Add("oauth_token", requestToken.Response.GetValue("oauth_token").ToString());
			payload.Add("oauth_version", OAuth.Version);

			string secret = this.Secret + "&" + requestToken.Response.GetValue ("oauth_token_secret");
			Signature signature = new Signature (secret, method, url, payload);
			payload.Add ("oauth_signature", signature.String());

			Response = Connection.Request (method, url, payload);
		}

		public JObject Consume(string resource)
		{
			return Consume (resource, "GET", null);
		}

		public JObject Consume(string resource, string method)
		{
			return Consume (resource, method, null);
		}

		public JObject Consume(string resource, string method, IDictionary<string, string> query)
		{
			string secret = this.Secret;
			string url = this.Host + "/" + resource;
			IDictionary<string, string> payload = new Dictionary<string, string> ();

			payload.Add ("oauth_consumer_key", Key);
			payload.Add ("oauth_nonce", OAuth.Nonce ());
			payload.Add ("oauth_signature_method", Signature.Method);
			payload.Add ("oauth_timestamp", OAuth.Timestamp ().ToString ());
			payload.Add ("oauth_version", OAuth.Version);

			JObject oauthToken = Response;
			string strQuery = null;

			if (oauthToken != null) {
				payload.Add ("oauth_token", oauthToken.GetValue ("oauth_token").ToString ());
				secret = secret + "&" + oauthToken.GetValue ("oauth_token_secret").ToString ();

				if (query != null) {
					query.ToList ().ForEach (x => payload [x.Key] = x.Value);
				}

			} else {

				if (query != null) {
					strQuery = Tools.DictionaryToString (query);
					payload.Add ("_d", strQuery);
				}
			}

			string signature = new Signature (secret, method, url, payload).String();
			payload.Add ("oauth_signature", signature);

			if (strQuery != null) {
				Tools.Log ("ANONYMOUS QUERY", strQuery);
				string encryptionKey = signature + secret;
				byte[] bytQuery = Tools.Encrypt (encryptionKey, Encoding.ASCII.GetBytes (strQuery));
				payload["_d"] = Tools.GetBase64(bytQuery);
			}

			return Connection.Request (method, url, payload);
		}
	}
}